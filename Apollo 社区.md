# Apollo 社区
## （一）社区简介
Apollo 是一个可靠的配置管理系统。它可以集中管理不同应用程序和不同集群的配置。它适用于微服务配置管理场景。Apollo 社区欢迎所有人以任何形式为社区做出贡献，包括但不限于文档改进、问题/错误提交、代码贡献、审查公关、技术讨论等，以共同促进开源生态系统的发展。

## （二）发展现状
目前，全国接近 [400](https://github.com/apolloconfig/apollo#known-users) 个企业级用户。目前拥有 PMC 成员：6 位，Committer：11 位，Contributors 接近 200 人。社区提供包括建立微信、QQ 群、邮件列表、GitHub issues/discussions等方式，与用户进行深入与及时的交流，更容易了解到用户与开发者的真实需求。

曾获得 2018 年最受欢迎服务架构
![输入图片说明](images/image25.png)

## （三）治理模式
Apollo 社区组成人员都来自不同的企业，自有一套独特的治理体系，其中包括 Users、Contributors、Committers、Project Management Committee (PMC) 四种角色。其中 PMC 主要负责日常管理、决策。Committers 为核心开发及协助运营。

## （四）运营实践
1.社区用户方面：截至 2022 年 7 月整个社区成员已至近 6,000 余人，社区提倡与用户共建，不断与用户进行深入地交流，倾听其真实需求，对产品进行不断优化

2.社区运营方面：Apollo 社区运营的不只是产品，更重要的是能快速满足用户需求的 [案例分享](https://github.com/apolloconfig/apollo-use-cases)、[最佳实践](https://www.apolloconfig.com/#/zh/usage/apollo-user-practices)、[安全相关实践](https://www.apolloconfig.com/#/zh/usage/apollo-user-guide?id=_71-%e5%ae%89%e5%85%a8%e7%9b%b8%e5%85%b3)。