# 中国开源社区 landscape

#### 介绍

中国开源社区 landscape。网罗中国优秀的开源社区，包括开源项目社区、综合性开源社区、官方背景相关开源社区等，群策群力，共筑开源生态。


#### 背景

OSCHINA 将在 7 月 27 日[开放原子开源峰会](https://www.openatom.org/summit)上（[https://www.openatom.org/summit](https://www.openatom.org/summit)），发布一个“中国开源社区 landscape”，我们希望组织起国内所有比较优秀的开源社区，包括开源项目社区、综合性开源社区、官方背景相关开源社区等。

后续这个项目会进行扩充和一系列推广运营，欢迎协作完善。

发布环节相关信息：

“中国开源社区 landscape” 将在峰会的《开源社区三十年专题活动》中作为一个环节发布。

- 时间：7 月 27 日上午 9-12 （**整个峰会时间为 7 月 25 日 - 27 日，为期 3 天**）
- 地点：北京市亦创国际会展中心

诚邀各社区朋友到现场参加共同见证。需要门票的朋友请添加小助手微信（x304561103），或直接扫描下方二维码进行添加：

![输入图片说明](images/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220716105257.png)

#### Oh

【欢迎提交 Pull Request 完善信息】

【欢迎提交 Pull Request 完善信息】

【欢迎提交 Pull Request 完善信息】

目前“[社区列表.md](/%E7%A4%BE%E5%8C%BA%E5%88%97%E8%A1%A8.md)”页面为社区汇总简介信息，在根目录可以另外自建社区专属文件，可以描述得更加详尽，包括社区发展历史、规章、组织架构、团队建设、协作模型……（每个社区的治理模式都不一样，自由发挥描述即可），参考根目录下的 “[PostgreSQL中⽂社区](/PostgreSQL%E4%B8%AD%E6%96%87%E7%A4%BE%E5%8C%BA.md)”等社区介绍。


**关于图片插入**：

- 请将 logo 图片存入 logo/ 目录，
- 其它图片存入 images/ 目录，
- 或者直接使用可用图片线上链接。

---

我们还组建了社区交流微信群，进群请添加小助手微信：x304561103，或直接扫描下方二维码进行添加：

![输入图片说明](images/%E5%BE%AE%E4%BF%A1%E6%88%AA%E5%9B%BE_20220716105257.png)

原则上社区中有代表进群即可，最佳模式：社区核心成员+运营

---

more：[开源项目社区健康案例合集](https://gitee.com/gitee-community/osscommunity-cases)
