## 中国开源软件推进联盟

中国开源软件推进联盟（China OSS Promotion Union，COPU）是在相关政策法规的指导下，由致力于开源软件文化、技术、产业、教学、应用的开源社区、大专院校、科研院所、企业等组织自愿组成的、民主议事的民间行业联合体，非独立社团法人组织。宗旨是推动中国开源软件（Linux/OSS）的发展和应用；促进中日韩以及中国与全球开源运动（Linux/OSS）的沟通、交流与合作；促进中国更好的为全球开源运动（Linux/OSS）做出贡献。联盟的作用是为推动Linux/OSS的发展，充分发挥联盟在政府与企业之间有关立法、政策、规 划和环境建设方面的桥梁、纽带与促进作用；充分发挥联盟在企业、社区、教育机构等组织团体之间的研发、生产、教育、测试认证、标准化、应用等方面交流、合作的桥梁与促进作用。

![输入图片说明](logo/image.png)

## 上海白玉兰开源开放研究院

白玉兰开源致力于与国内外知名开源社区互联互通，汇聚国内外开发者的智慧，以开源社区平台为牵引，以提升先进算法和模型的可复现性为目标，推动人工智能领域开源软件的国际规则互认，在重点领域形成“算力、算法、数据、场景、合规”一体化的人工智能社区，建设国际人工智能开发生态网络的关键节点。白玉兰开源借助人工智能应用场景的开源开放运行机制，为人工智能领域的成果转化和产业创新提供坚实的系统基础。帮助中小企业和科研机构利用巨头企业的开源开放框架服务传统行业，并创建一套符合中国语境和法律环境的标准授权许可或标准协议。

## CCF 开源发展委员会

CCF创建开源发展委员会（CCF Opensource Development Committee, CCF ODC），是CCF落实国家十四五规划纲要提出的“支持数字技术开源社区等创新联合体发展”的重要举措，旨在构建产学研项目成果共享孵化机制，加速产教研深度融合的开源生态建设。

CCF开源发展委员会将重点聚焦共同打造开源、开放、中立的产学研协同开源创新服务平台，探索建立CCF开源项目孵化机制，培育原始开源创新项目，依托CCF联接科教资源、产业资源和社会资源等，形成产、教、研联动的开源创新模式，推动CCF开源品牌建设和开源社区运营，为CCF会员乃至全球开源创新实践者提供高水平的开源创新服务，助力我国开源生态建设的发展。

## 中国开源云联盟

中国开源云联盟 (COSCL) 成立于 2012 年 8 月，在工业和信息化部信息技术发展司指导下，挂靠在中国电子技术标准化研究院，现有我国 200 余家开源生态圈产、学、研、用成员单位。中国开源云联盟作为国家重点研发计划《云计算和大数据开源社区生态系统》项目的指定开源组织，宗旨是以标准化为核心，汇聚产业能力，发挥开源智库作用，服务开源产业各界及地方政府。

## 木兰开源社区

“木兰开源社区”建立于 2019 年 8 月，是国家重点研发计划重点专项“云计算和大数据开源社区生态系统”的核心成果。旨在促进产学研用各方开源领域的交流，推动国家科技创新成果开源，加强企业、科教研单位和行业用户之间的沟通，推动开源成果转化落地，同时为各类开源项目提供中立托管，保证开源项目的持续发展不受第三方影响，通过更加开放的方式来打造和完善开源社区生态。

## OSCHINA

[开源中国 (OSCHINA)](https://www.oschina.net) 成立于 2008 年 8 月，是国内知名开源技术社区。网站用户超 500w，日活用户 150w+。形成了由开源软件库、问答、资讯、讨论区、博客、代码托管与 DevOps 工具等几大频道内容，为 IT 开发者提供了一个发现、使用、并交流开源技术的平台。

![输入图片说明](logo/oschina.png)

## 星策开源社区

星策开源社区成立于 2022 年 3 月，由中国开源软件推进联盟、中国信通院、Linux 基金会 AI & DATA、腾讯、微众银行、第四范式等多家行业领军企业及机构共同发起，是一个非商业属性的中立的企业智能转型开源社区，共享共建企业智能转型的方法论、案例、最佳实践和工具平台，助力中国企业智能转型。

![星策社区logo](logo/startogether-logo.png)

## 开源社

开源社成立于 2014 年，是由志愿贡献于开源事业的个人成员，依 “贡献、共识、共治” 原则所组成，始终维持厂商中立、公益、非营利的特点，是最早以 “开源治理、国际接轨、社区发展、开源项目” 为使命的开源社区联合体。开源社积极与支持开源的社区、企业以及政府相关单位紧密合作，以 “立足中国、贡献全球” 为愿景，旨在共创健康可持续发展的开源生态，推动中国开源社区成为全球开源体系的积极参与及贡献者。

2017 年，开源社转型为完全由个人成员组成，参照 ASF 等国际顶级开源基金会的治理模式运作。近七年来，链接了数万名开源人，集聚了上千名社区成员及志愿者、海内外数百位讲师，合作了近百家赞助、媒体、社区伙伴。

## ALC Beijing、ALC Shenzhen	
Apache Local Community 由一群分布在各地的开源爱好者，尤其是 Apache（开源）爱好者（Group）所组成，因为是本地组织，所以 ALC 是按照城市或地区的方式进行划分的。因此顾名思义，ALC Beijing 就是北京本地开源爱好者组成的社区，ALC Shenzhen 即为深圳地区团体。他们希望通过这种方式向更多人介绍 Apache 之道，传播 Apache 精神，让更多的年轻人认识 Apache 和开源之道， 让本土的项目被更多的人知道。

## NGINX 开源社区
NGINX 开源社区创建于2020年6月，是在 NGINX 官方直接支持下创建的的全球第一个服务于普通 NGINX 用户和开发者的全中文社区。社区秉持着“开放，包容，沟通，贡献“的宗旨，致力于打造一个学习进步、深度交流、互动讨论、自我成长的平台。作为一个技术知识和案例分享的集结地，NGINX 中文开源社区已经积累了注册用户 1600 余成员，微信群用户 6000 余人，且社区规模仍在快速增长。

## MongoDB 中文社区
MongoDB 中文社区 (mongoing.com) 成立于 2014 年，是围绕 MongoDB 生态建立的技术交流平台。社区由技术博客、技术大会、线上活动、技术论坛、技术培训、官方文档翻译等版块组成，迄今为止已经举办了数十场线下活动和线上直播，发表了数百篇技术文章及文档，在社区里支持了数万计的 MongoDB 用户。

## Jenkins 中文社区
Jenkins 中文社区是由 Jenkins 国内的爱好者、贡献者组成，共同推广以及完善 CI/CD 技术的学习试用和落地。我们秉承开源的精神，在社区治理上高度开放，代码、文档以及设计等开源免费，致力于为我们的用户带来更好的体验。

## CPyUG（Python 中国用户组）
又称华蟒用户组，蟒取自 Python 的本意，它主要“面向实习、应用、推广 Python 以及相关技术的爱好者”。

## Linux 中国
Linux 中国是一个聚焦在 Linux 的开源社区，发展方向为：推动 Linux 和开源在中国的发展，促进 Linux 及开源技术在国内的普及，构建开源社区环境。Linux 中国的发展宗旨是：“成为华文区一流的 Linux 与开源交流社区”

## Ruby China 社区	
Ruby China Group 是一个非营利组织，它旨在为中国的 Ruby 和 Rails 爱好者提供一个自由，开放的交流平台。

## [PostgreSQL 中文社区](/PostgreSQL%E4%B8%AD%E6%96%87%E7%A4%BE%E5%8C%BA.md)	
自1993年起，热心网友何伟平（网名laser）开始独自翻译并研究PostgreSQL十余年，相关资料在网上广为流传，为广大中国IT开发人员了解PostgreSQL起到了重要的作用。随着众多使用和爱好PostgreSQL人员的加入，PostgreSQL在中国的应用呈快速发展之势。为进一步规范中文社区的发展，2013杭州PostgreSQL用户大会上，经广大网友的一致讨论，正式成立注册的组织---PostgreSQL中国用户协会，并根据各位网友的特长和爱好，成立不同的事务处理小组和核心的管理委员会，共同努力更好地为广大网友服务，在中国大力推广这一优秀的数据库软件。

## FreeSWITCH 中文社区	
FreeSWITCH-CN 是 FreeSWITCH 中文站、FreeSWITCH 中文社区的简称，旨在为广大 FreeSWITCH 中文用户提供一个统一的登陆页，便于大家查找 FreeSWITCH 相关的中文信息、促进大家的技术交流以及 FreeSWITCH 技术在中国的传播。

## openKylin 社区	
openKylin 社区是在开源、自愿、平等和协作的基础上，由基础软硬件企业、非营利性组织、社团组织、高等院校、科研机构和个人开发者共同创立的一个开源社区，致力于通过开源、开放的社区合作，构建桌面操作系统开源社区，推动Linux开源技术及其软硬件生态繁荣发展。

## 开源项目技术传播社区

开源项目技术传播社区，讨论技术传播相关的事情，包括并不限于技术文档写作、布道、演讲等。

## 开放群岛	
开放群岛（Open Islands）开源社区是由深圳数据交易有限公司联合国家智库、国家单位、高校、大型金融机构、大型互联网公司等50家发起单位牵头成立，将充分利用深圳自主创新的先进机制和产业优势，以服务全国数据要素流通应用场景为目标，打造 “坚持信创、场景主导、互联互通、开源开放”社区特点，助力加快建设全国数据交易统一大市场，以开源开放的方式充分整合政、产、学、研、用等多方资源，推动数据要素流通关键基础技术发展。

## OpenTEKr	
OpenTEKr 是一家以推广开源软件和开放硬件技术为核心的开放式非营利组织，致力于构建可持续发展的开放科技生态圈。基于“众有、众享、众治”的信念，依循“自由与规则同在，免费与商业共生”的原则，憧憬科技普惠的美好未来，帮助个人和组织通过变革性的技术创新来成就其远大抱负。

## 腾源会	
腾源会是腾讯云成立的汇聚开源项目、开源爱好者、开源领导者的开放社区，致力于帮助开源项目健康成长、开源爱好者能交流协助、开源领导者能发挥领袖价值，让全球开源生态变得更加繁荣。

## [云智慧AIOps社区](https://www.cloudwise.ai/)
AIOps社区是由云智慧发起，针对运维业务场景，提供算法、算力、数据集整体的服务体系及智能运维业务场景的解决方案交流社区。 社区致力于传播AIOps技术，旨在与各行业客户、用户、研究者和开发者们共同解决智能运维行业技术难题、推动AIOps技术在企业中落地、建设健康共赢的AIOps开发者生态。
![输入图片说明](logo/screenshot-20220715-160403.png)

## [Dromara社区](/Dromara%20社区.md)       
Dromara 是由国内顶尖的开源项目作者共同组成的开源社区。提供包括分布式事务，流行工具，企业级认证，微服务RPC，运维监控，Agent监控，分布式日志，调度编排等一系列开源产品、解决方案与咨询、技术支持与培训认证服务。技术栈全面开源共建、 保持社区中立，致力于为全球用户提供微服务云原生解决方案。让参与的每一位开源爱好者，体会到开源的快乐。

## 携程 Apollo	

Apollo（阿波罗）是一款可靠的分布式配置管理中心，诞生于携程框架研发部，能够集中化管理应用不同环境、不同集群的配置，配置修改后能够实时推送到应用端，并且具备规范的权限、流程治理等特性，适用于微服务配置管理场景。

## 香山	

香山是一款开源的高性能 RISC-V 处理器，基于 Chisel 硬件设计语言实现，支持 RV64GC 指令集。在香山处理器的开发过程中，团队使用了包括 Chisel、Verilator 等在内的大量开源工具，实现了差分验证、仿真快照、RISC-V 检查点等处理器开发的基础工具，建立起了一套包含设计、实现、验证等在内的基于开源工具的处理器前端敏捷开发流程。

## 禅道	

禅道是一款项目管理软件，她的核心管理思想基于敏捷方法 scrum，内置了产品管理和项目管理，同时又根据国内研发现状补充了测试管理、计划管理、发布管理、文档管理、事务管理等功能，在一个软件中就可以将软件研发中的需求、任务、bug、用例、计划、发布等要素有序的跟踪管理起来，完整地覆盖了项目管理的核心流程。

## 百度 Apollo	
Apollo (阿波罗) 是一个开放的、完整的、安全的平台，将帮助汽车行业及自动驾驶领域的合作伙伴结合车辆和硬件系统，快速搭建一套属于自己的自动驾驶系统。

## ZStack	
ZStack 是开源 IaaS 软件，它的诞生是为了解决困绕 IaaS 软件的几大难题：复杂度、稳定性、可伸缩性和灵活性。

## ZNBase	
ZNBase 是浪潮打造的一款分布式数据库产品，具备强一致、高可用分布式架构、分布式水平扩展、高性能、企业级安全等特性，自研的原生分布式存储引擎支持完整 ACID，支持 PostgreSQL 协议访问。同时提供自动化运维、监控告警等配套服务，为用户提供完整的分布式数据库解决方案。

## zlog	
zlog 是一个高性能、线程安全、灵活、概念清晰的纯 C 日志函数库。

## Zadig	
Zadig 是一款面向开发者设计的云原生持续交付 (Continuous Delivery) 产品，具备高可用 CI/CD 能力，提供云原生运行环境，支持开发者本地联调、微服务并行构建和部署、集成测试等。Zadig 不改变现有流程，无缝集成 Github/Gitlab、Jenkins、多家云厂商等，运维成本极低。

## YoMo	
YoMo 是一套开源的实时边缘计算 Streaming Serverless 开发框架，通讯层基于 QUIC 协议，以 FRP 为编程范式，更好的释放了未来 5G 等低时延网络的价值；为流式处理（Streaming Computing）设计的编解码器 yomo-codec 能大幅提升计算服务的吞吐量；基于插件的开发模式，5 分钟即可上线用户的物联网实时边缘计算处理系统。

## Yaf	
Yaf 是一个 C 语言编写的 PHP 框架。

## XXL-JOB	
XXL-JOB 是一个轻量级分布式任务调度平台，其核心设计目标是开发迅速、学习简单、轻量级、易扩展。现已开放源代码并接入多家公司线上产品线，开箱即用。

## XuperChain	
XuperChain 是一种区块链技术。拥有链内并行技术、可插拔共识机制、一体化智能合约等业内领先技术支撑，让区块链应用搭建更灵活、性能更高效、安全性更强，全面赋能区块链开发者

## xmake	
xmake 是一个基于 Lua 的轻量级跨平台构建工具，使用 xmake.lua 维护项目构建，相比 makefile/CMakeLists.txt，配置语法更加简洁直观，对新手非常友好，短时间内就能快速入门，能够让用户把更多的精力集中在实际的项目开发上。

## XGVela	
XGVela 项目计划将电信类业务及系统所需的公共能力沉淀至平台侧形成 PaaS 能力，以便帮助运营商快速设计、开发、创新和管理电信类业务及系统。

## X-DeepLearning (XDL)	
X-DeepLearning (简称 XDL) 是面向高维稀疏数据场景（如广告 / 推荐 / 搜索等）深度优化的一整套解决方案。
现有开源框架在分布式性能、计算效率、水平扩展能力以及实时系统适配性的等方面往往难以满足工业级生产应用的需求，XDL 正是面向这样的场景设计与优化的工业级深度学习框架，经过阿里巴巴广告业务的锤炼，XDL 在训练规模和性能、水平扩展能力上都表现出色，同时内置了大量的面向广告 / 推荐 / 搜索领域的工业级算法解决方案。

## WeCube	
WeCube 是一套开源的，一站式 IT 架构管理和运维管理工具，主要用于简化分布式架构 IT 管理，并可以通过插件进行功能扩展。

## Wechaty	
Wechaty 是针对微信个人帐户的会话式 AI RPA（Robotic Process Automation，机器人流程自动化）聊天机器人 SDK，使用 JavaScript、Python、Go 或 Java 仅用 6 行代码即可创建机器人。支持 Linux、Windows、macOS 和 Docker 等平台。

## Wayne	
Wayne 是一个通用的、基于 Web 的 Kubernetes 多集群管理平台。通过可视化 Kubernetes 对象模板编辑的方式，降低业务接入成本， 拥有完整的权限管理系统，适应多租户场景，是一款适合企业级集群使用的发布平台。

## WasmEdge	
[WasmEdge](https://wasmedge.org/)（以前称为 SSVM）是 CNCF 沙箱项目，是一个安全、轻量级、高性能、可扩展的 WebAssembly Runtime。支持使用 K8s 及其生工具编排、支持 JavaScript 与 Networking Socket、TensorFlow AI 推理，是一个功能完备的 WebAssembly runtime。

![](logo/wasm-edge-runtime-horizontal-color.png)

## W5 SOAR	
W5 是一款基于 Python 开发的安全编排与自动化响应平台，为了企业安全做出了精心的打造，无需编写代码即可实现自动化响应流程，可节约企业 80% 的成本。

## Vue.js	
Vue.js 是构建 Web 界面的 JavaScript 库，提供数据驱动的组件，还有简单灵活的 API，使得 MVVM 更简单。

## Vant	

Vant 是有赞前端团队开源的移动端组件库，Vant 对内承载了有赞所有核心业务，对外服务十多万开发者，是业界主流的移动端组件库之一。

## uni-app	
uni-app 是一个使用 Vue.js 开发所有前端应用的框架，开发者编写一套代码，可发布到 iOS、Android、Web（响应式）、以及各种小程序（微信 / 支付宝 / 百度 / 头条 / QQ / 钉钉 / 淘宝）、快应用等多个平台。

## Ubuntu Kylin	
优麒麟 (Ubuntu Kylin) 社区成立于 2013 年，是围绕优麒麟操作系统及应用生态建立的技术交流平台。社区由线上线下技术交流活动、Q&A 反馈论坛、开发者平台、文档翻译平台等板块组成，我们奉行开源精神，累计向开源社区贡献代码数百万行，其中被 Linux、GNOME、Unity、OpenStack、Ceph 等开源社区接收 patch 超 7320 个，全球累计下载量超 3560 万次，活跃爱好者和开发者数十万人，并累计在全球 30+ 城市、50+ 大学举行 100+ 场线下活动，已进入多所高校教学课堂和课程实践，培养 Linux 人才数万人。

## UBML	

UBML 是一种基于领域特定语言（Domain-Specific Language DSL）的、用于快速构建应用软件的低代码建模语言。内容包括模型标准及其默认实现、SDK、运行时框架等组件。UBML 定位于 APaaS （应用程序平台即服务）领域，是低代码开发平台（Low-Code-Development-Platform）的核心基础，致力于在低代码领域建立应用软件建模开发的事实标准。

## TKEStack	

TKEStack 是基于 Kubernetes 的开源容器平台，是腾讯面向离线计算和在线业务混合部署场景推出的 Kubernetes 发行版。

## TinyFramework	

TinyFramework 是一个 J2EE 应用开发框架

## TiKV	
TiKV 是一个开源的分布式事务 Key-Value 数据库，支持跨行 ACID 事务，同时实现了自动水平伸缩、数据强一致性、跨数据中心高可用和云原生等重要特性。作为一个基础组件，TiKV 可作为构建其它系统的基石。
## TiDB	
TiDB 是一款定位于在线事务处理 / 在线分析处理（ HTAP: Hybrid Transactional/Analytical Processing）的融合型数据库产品，实现了一键水平伸缩，强一致性的多副本数据安全，分布式事务，实时 OLAP 等重要特性。同时兼容 MySQL 协议和生态，迁移便捷，运维成本极低。

## ThinkPHP	
ThinkPHP 是一个快速、简单的面向对象的轻量级 PHP 开发框架，创立于 2006 年初，遵循 Apache2 开源协议发布，是为了敏捷 WEB 应用开发和简化企业应用开发而诞生的。

## TencentOS Tiny	

TencentOS tiny 是腾讯面向物联网领域开发的实时操作系统，具有低功耗，低资源占用，模块化，安全可靠等特点，可有效提升物联网终端产品开发效率。TencentOS tiny 提供精简的 RTOS 内核，内核组件可裁剪可配置，可快速移植到多种主流 MCU (如 STM32 全系列) 及模组芯片上。而且，基于 RTOS 内核提供了丰富的物联网组件，内部集成主流物联网协议栈（如 CoAP/MQTT/TLS/DTLS/LoRaWAN/NB-IoT 等），可助力物联网终端设备及业务快速接入腾讯云物联网平台。

## TDesign	
TDesign 是一款诞生于腾讯内部、拥有完整的设计价值观和视觉风格指南的企业级设计体系，同时提供了丰富的设计资源，在设计体系基础上产出基于 Vue、React、小程序等业界主流技术栈的组件库解决方案，适合用于构建设计统一 / 多端覆盖 / 跨技术栈的企业级前端应用。
## TDengine	
TDengine 是专为物联网、车联网、工业互联网、IT 运维等设计和优化的大数据平台。除核心的快 10 倍以上的时序数据库功能外，还提供缓存、数据订阅、流式计算等功能，最大程度减少研发和运维的复杂度，且核心代码，包括集群功能全部开源。

## TBase	

TBase 是腾讯数据平台团队在开源的 PostgreSQL 基础上研发的企业级分布式 HTAP 数据库管理系统

## TARS	

TARS 是基于名字服务使用 TARS 协议的高性能 RPC 开发框架，同时配套一体化的服务治理平台，帮助个人或者企业快速的以微服务的方式构建自己稳定可靠的分布式应用。
## Taro	
Taro 是开放式跨端跨框架解决方案，支持使用 React/Vue/Nerv 等框架来开发微信 / 京东 / 百度 / 支付宝 / 字节跳动 / QQ 小程序 / H5 等应用。
## Taichi	
Taichi (太极) 是专为高性能计算机图形学设计的编程语言。 它深深地嵌入在 Python 中，并且它的即时编译器将计算密集型任务转移到多核 CPU 和大规模并行 GPU 上。


## Swoole	
Swoole 是一个使用 C++ 语言编写的基于异步事件驱动和协程的并行网络通信引擎，为 PHP 提供协程、高性能网络编程支持。提供了多种通信协议的网络服务器和客户端模块，可以方便快速的实现 TCP/UDP服务、高性能Web、WebSocket服务、物联网、实时通讯、游戏、微服务等，使 PHP 不再局限于传统的 Web 领域。
## StreamX	
StreamX 是一个 Apache Flink 极速开发框架。项目的初衷是 —— 让 Flink 开发更简单。
## StarRocks 中文社区	
StarRocks 是新一代极速全场景 MPP 数据库。StarRocks 的愿景是能够让用户的数据分析变得更加简单和敏捷。用户无需经过复杂的预处理，就可以用 StarRocks 来支持多种数据分析场景的极速分析。

![](https://docs.starrocks.com/static/b660bcde69091ea56bd94cac0a907018/95f17/starrocks-logo_zh-cn.png)
## SRS	
SRS 是是一个简单高效的实时视频服务器，支持 RTMP/WebRTC/HLS/HTTP-FLV/SRT/GB28181。
## Spring Cloud Alibaba	
Spring Cloud Alibaba 致力于提供分布式应用服务开发的一站式解决方案。此项目包含开发分布式应用服务的必需组件，方便开发者通过 Spring Cloud 编程模型轻松使用这些组件来开发分布式应用服务。

## [SOFAStack](/SOFAStack%20开源社区.md)

SOFAStack(Scalable Open Financial Architecture Stack) 是一套用于快速构建金融级云原生架构的中间件，也是在金融场景里锤炼出来的最佳实践，并且具备以下特点：

- 开放：技术栈全面开源共建、 保持社区中立、兼容社区 兼容开源生态，组件可插拔， SOFAStack 组件与其它开源组件可相互集成或替换;
- 金融级：包含构建金融级云原生架构所需的各个组件，让用户更加专注于业务开发，满足用户场景的现状和未来需求，经历过大规模场景的锤炼，特别是严苛的金融场景;
- 云原生：基于 SOFAStack 可快速搭建云原生微服务体系，快速开发更具可靠性和扩展性、更加易于维护的云原生应用

## Slime	
Slime 是网易数帆旗下轻舟微服务团队开源的服务网格组件，它可以作为 Istio 的 CRD 管理器，旨在通过更为简单的配置实现 Istio/Envoy 的高阶功能。

![输入图片说明](logo/slime.png)

## SequoiaDB	
SequoiaDB 巨杉数据库是一款开源的金融级分布式关系型数据库，主要面对高并发联机交易型场景提供高性能、可靠稳定以及无限水平扩展的数据库服务。
## Semi Design	
Semi Design 是现代、全面、灵活的设计系统和 UI 库，由字节跳动抖音前端与 UED 团队设计、开发并维护，是一款包含设计语言、React 组件、主题等开箱即用的中后台解决方案，可用于快速搭建美观的 React 应用。
## RT-Thread	
RT-Thread 是一个集实时操作系统（RTOS）内核、中间件组件和开发者社区于一体的技术平台，具有极小内核、稳定可靠、简单易用、高度可伸缩、组件丰富等特点。RT-Thread 拥有一个国内最大的嵌入式开源社区，同时被广泛应用于能源、车载、医疗、消费电子等多个行业，累积装机量达数千万台，成为国人自主开发、国内最成熟稳定和装机量最大的开源 RTOS。


## Rancher	
Rancher 是一个开源的项目，提供了在产品环境中对 Docker 容器进行全方位管理的平台。它提供的基础架构服务包括多主机网络、全局和局部的负载均衡、卷快照等。集成了原生 Docker 管理能力，包括：Docker Machine 和 Docker Swarm。Rancher 还提供了丰富用户体验的管理功能。

## Rainbond	

Rainbond 是一个云原生应用管理平台，使用简单，不需要懂容器、Kubernetes和底层复杂技术，支持管理多个Kubernetes集群，和管理企业应用全生命周期。主要功能包括应用开发环境、应用市场、微服务架构、应用交付、应用运维、应用级多云管理等。

![输入图片说明](logo/rainbond.png)


## Proxy-Go	
Proxy-Go 是 golang 实现的高性能 http、https、websocket、tcp、udp、socks5 代理服务器，支持正向代理、反向代理、透明代理、内网穿透、TCP/UDP 端口映射、SSH 中转、TLS 加密传输、协议转换、DNS 防污染智能代理、前置 CDN/Nginx 反代、代理连接重定向、API 动态调用上级代理、限速限连接数。同时提供全平台的功能强大的命令行版本和友好易用的 web 控制面板版本。
## Pika	
Pika 是 360 DBA 和基础架构组联合开发的类 Redis 存储系统，完全支持 Redis 协议，用户不需要修改任何代码，就可以将服务迁移至 Pika。有维护 Redis 经验的 DBA 维护 Pika 不需要学习成本。
## Pigsty	
[Pigsty ](https://pigsty.cc/zh/docs/feature/)是开箱即用的 PostgreSQL 数据库发行版，帮助用户用好开源数据库。

Pigsty允许用户在本地一键拉起生产级 RDS 服务。Pigsty以 PostgreSQL 为核心，打包TimescaleDB，PostGIS，Citus与上百余+生态扩展插件， 整合了大规模生产环境所需的PaaS基础设施与数据分析组件。

Pigsty还是自动驾驶的运维解决方案，带有全面专业的监控系统，与简单易用的高可用数据库部署管控方案。用户只需声明自己想要什么样的数据库，即可将其一键创建：PostgreSQL / Redis / Greenplum 。

Pigsty是简单易用的开发者工具箱，Database as Code 无论是下载、安装、还是部署迁移备份恢复扩缩容，都能一键完成。基于Vagrant的本地沙箱与Terraform的多云部署能力，让Pigsty在所有环境中都能一键拉起，带来统一的使用体验。

Pigsty用途广泛，可支持各类上层SaaS应用或制作大屏/Demo。相比使用云数据库，数据安全自主可控，简运维、低成本、全功能、优体验，可以显著节省数据库运维人力，并节约 50% ~ 80% 的数据库综合成本。对各类企业用户、ISV、个人用户都具有显著的价值与吸引力。

![](logo/pigsty.png)



## pig	
pig 是一个基于 Spring Cloud 的企业级认证与授权框架
## PiFlow	
PiFlow 是一个基于分布式计算框架 Spark 开发的大数据流水线系统。该系统将数据的采集、清洗、计算、存储等各个环节封装成组件，以所见即所得方式进行流水线配置。
## PDManer	
PDManer 元数建模，是一款多操作系统开源免费的桌面版关系数据库模型建模工具，相对于 PowerDesigner，他具备界面简洁美观，操作简单，上手容易等特点。
## PaddlePaddle	
飞桨PaddlePaddle作为我国首个自主研发、功能完备、开源开放的产业级深度学习平台，具有开发便捷的核心框架、支持超大规模深度学习模型训练、多端多平台部署的高性能推理引擎和覆盖多领域的产业级模型库等四大核心技术。目前社区已凝聚 320 万开发者，累计 commits 超过 16 万次，以 PR 或 ISSUE 提交形式的开源贡献者超过了 5000 位，其中超过 130 位优秀的开发者通过层层筛选成为了百度飞桨开发者技术专家 (PPDE)。产业应用方面，飞桨PaddlePaddle服务了 12 万家企业，平台上创建了 36 万个模型。
## OpenYurt	
OpenYurt 是一个边缘计算云原生项目。该项目主打 “云边一体化” 概念，依托原生 Kubernetes 强大的容器编排、调度能力，通过众多边缘计算应用场景锤炼，实现了一整套对原生 Kubernetes “零” 侵入的边缘云原生方案，提供诸如边缘自治、高效运维通道、边缘单元化管理、边缘流量拓扑管理，安全容器、边缘 Serverless/FaaS、异构资源支持等能力。
## OpenResty	
OpenResty（也称为 ngx_openresty）是一个基于 Nginx 与 Lua 的高性能 Web 平台，其内部集成了大量精良的 Lua 库、第三方模块以及大多数的依赖项。用于方便地搭建能够处理超高并发、扩展性极高的动态 Web 应用、Web 服务和动态网关。
## OpenMLDB	
OpenMLDB 是一个面向机器学习应用提供正确、高效数据供给的开源数据库。除了超过 10 倍的机器学习数据开发效率的提升，OpenMLDB 也提供了统一的计算与存储引擎减少开发运维的复杂性与总体成本。
## openLooKeng	
openLooKeng 是一款高性能数据虚拟化引擎，提供统一 SQL 接口，具备跨数据源 / 数据中心分析能力以及面向交互式、批、流等融合查询场景。同时增强了前置调度、跨源索引、动态过滤、跨源协同、水平拓展等能力。
## OpenHarmony	
OpenHarmony 是一款面向全场景的开源分布式操作系统。OpenHarmony 在传统的单设备系统能力的基础上，创造性地提出了基于同一套系统能力、适配多种终端形态的理念，支持多种终端设备上运行。

## [openGauss](/openGauss%20社区.md)	

openGauss 是一款开源关系型数据库管理系统，深度融合华为在数据库领域多年的经验，结合企业级场景需求，持续构建竞争力特性，具有高可靠、高安全、高性能与易运维等特点。

## openEuler	
openEuler 操作系统面向企业级通用服务器架构平台，支持鲲鹏处理器和容器虚拟化技术，特性包括系统高可靠、高安全以及高保障。
## OpenCloudOS	
OpenCloudOS 是一个开源操作系统社区
## OpenBlock(待更名)	
OpenBlock 是面向业务的、基于状态机编程的图形化编程语言，以Blockly为前端，提供了完整IDE支持，内部实现了类型系统、语法树、编译、字节码、运行时等现代语言的全部技术核心。
OpenBlock编辑器原生使用HTML5技术构建，可部署于任何服务器并在任何主流浏览器中运行。
OpenBlock精简的指令集，支持多宿主语言、可跨平台部署；支持高并发、多线程。
面向状态机的编程范式，降低代码低耦合，拥有快速构建可扩、展易维护的业务模型的能力。 

## OneFlow	
OneFlow 是一个采用全新架构设计的工业级通用深度学习框架。
OneFlow 率先提出了静态调度和流式执行的核心理念，解决了大数据、大模型、大计算带来的异构集群分布式扩展挑战，具有五大优势：并行模式全、运行效率高、分布式易用、资源节省、稳定性强。
## OMP	
OMP（Operation Management Platform）是轻量级、聚合型、智能运维管理平台。是一款为用户提供便捷运维能力和业务管理的综合平台。具备运维一应俱全的功能，目的是提升运维自动化、智能化，提高运维效率，提升业务连续性和安全性。
## [OceanBase](/OceanBase%20社区.md)	
OceanBase 数据库是蚂蚁集团自主研发的原生分布式关系数据库，提供金融级高可用和线性伸缩能力，不依赖特定硬件架构，具备高可用、线性扩展、高性能、低成本等核心技术优势。
## Nutz	 
Nutz 不只是一个 Web 框架，更是一种信仰。在力所能及的情况下，最大限度的提高 Java 开发人员的生产力。
## Notadd	
Notadd 是一个开源的、基于 Nest.js 框架的微服务开发架构，您可以根据不同的业务需求使用适合的模块、插件来搭建一个微服务系统。Notadd 官方提供了抽象化的公共服务层，在服务层内，每一个模块都提供了 Grpc 接口供 Notadd 主程序进行调用。
## NoForm	
NoForm 是阿里巴巴外综服前端团队在外综服（外贸综合服务）场景下，经过长期的思考和打磨产出的一款基于 React 的表单解决方案。
## NebulaGraph	
Nebula 是一个分布式、可扩展的图数据库。它是世界上唯一能够容纳具有数十亿个顶点（节点）和数万亿个边缘的图数据库解决方案，同时仍然提供毫秒延迟。
Nebula 的目标是为超大规模图提供高并发，低延迟的读、写和计算。

## ncnn	
ncnn 是腾讯优图实验室首个开源项目，是一个为手机端极致优化的高性能神经网络前向计算框架。
ncnn 从设计之初深刻考虑手机端的部属和使用。无第三方依赖，跨平台，手机端 cpu 的速度快于目前所有已知的开源框架。
基于 ncnn，开发者能够将深度学习算法轻松移植到手机端高效执行，开发出人工智能 APP，将 AI 带到你的指尖。

## Nacos	

Nacos 致力于帮助您发现、配置和管理微服务。Nacos 提供了一组简单易用的特性集，帮助您实现动态服务发现、服务配置管理、服务及流量管理。
## MyCAT	
MyCAT 是一个面向企业应用开发的 “大数据库集群” 支持事务、ACID、可以替代 Mysql 的加强版数据库
## MiniGUI	
MiniGUI 是一个自由软件项目。其目标是提供一个快速、稳定、跨操作系统的图形用户界面（GUI）支持系统，尤其是基于 Linux/uClinux、eCos 以及其他传统 RTOS（如 VxWorks、ThreadX、uC/OS-II、Nucleus 等）的实时嵌入式操作系统。

## [MindSpore](/MindSpore%20社区.md)	
MindSpore 是一种适用于端边云场景的新型开源深度学习训练/推理框架，旨在提升数据科学家和算法工程师的开发体验，并为 Ascend AI 处理器提供原生支持，以及软硬件协同优化。
## Milvus	
Milvus 向量数据库能够帮助用户轻松应对海量非结构化数据（图片 / 视频 / 语音 / 文本）检索。单节点 Milvus 可以在秒内完成十亿级的向量搜索，分布式架构亦能满足用户的水平扩展需求。
## mica	
mica 云母，寓意为云服务的核心，使得云服务开发更加方便快捷。mica 的前身是 lutool，lutool 在内部孵化了小两年，已经被多个朋友运用到企业。
## Metrik	
Metrik 是自动化 DevOps 度量工具，内建 Dashboard。可以从持续集成（Jenkins、Bamboo 等）、持续交付流水线中提取数据，分析四项关键 DevOps 度量指标：部署频率、变更前置时间、服务恢复时间、变更失败率。
## MeterSphere	
MeterSphere 是一站式的开源企业级持续测试平台，涵盖测试跟踪、接口测试、性能测试、 团队协作等功能，兼容 JMeter 等开源标准，有效助力开发和测试团队充分利用云弹性进行高度可扩展的自动化测试，加速高质量的软件交付，推动中国测试行业整体效率的提升。


## MegEngine	
MegEngine（天元）是一个快速，可扩展，易于使用且支持自动求导的深度学习框架，中文名为 “天元”，是旷视 AI 战略的重要组成部分，负责 AI 三要素（算法，算力，数据）中的 “算法”。
## MatrixOne	
MatrixOne 是一个行星级的云端原生大数据引擎，专为异构工作负载而设计。它提供了一个端到端的数据处理平台，具有高度的自主性和易用性，使用户能够跨设备、边缘和云来存储、处理和分析数据，并将操作费用降到最低。
## MACE	
Mobile AI Compute Engine (MACE) 是一个专为移动端异构计算平台优化的神经网络计算框架。 
## LiteOS	
LiteOS 是华为面向物联网领域开发的一个基于实时内核的轻量级操作系统。现有基础内核支持任务管理、内存管理、时间管理、通信机制、中断管理、队列管理、事件管理、定时器等操作系统基础组件，更好地支持低功耗场景，支持 tickless 机制，支持定时器对齐。
## LiteIDE	
LiteIDE 是一款开源、跨平台的轻量级 Go 语言集成开发环境（IDE）。

## Linux Lab	
Linux Lab 由泰晓科技 Linux 技术社区研发，是一套基于 Docker & Qemu 的极速 Linux 内核学习、开发与测试环境，也可用于嵌入式 Linux 系统开发。

![Linux Lab Logo](https://gitee.com/tinylab/linux-lab/raw/master/doc/images/linux-lab-logo.jpg)

## LCUI	
LCUI 是一个用 C 语言编写的图形界面开发库，支持使用 CSS 和 XML 描述界面布局和样式，可用于构建简单的桌面应用程序。
## LayX	
Layx 是一款仿 Windows 系统窗口的 Web 弹窗插件。gzip 压缩版仅 13.5kb，非常小巧。
## LayUI	
LayUI 是一套开源的 Web UI 组件库，采用自身经典的模块化规范，并遵循原生 HTML/CSS/JS 的开发方式，极易上手，拿来即用。其风格简约轻盈，而组件优雅丰盈，从源代码到使用方法的每一处细节都经过精心雕琢，非常适合网页界面的快速开发。layui 区别于那些基于 MVVM 底层的前端框架，却并非逆道而行，而是信奉返璞归真之道。准确地说，它更多是面向后端开发者，你无需涉足前端的各种工具，只需面对浏览器本身，让一切你所需要的元素与交互，从这里信手拈来。
## KubeVela	
KubeVela 是一个简单易用且高度可扩展的应用管理平台与核心引擎。KubeVela 是基于 Kubernetes 与 OAM 技术构建的。
## KubeSphere	
KubeSphere 是基于 Kubernetes 构建的多租户、企业级容器平台，具有强大且完善的网络与存储能力，并通过极简的人机交互提供完善的 CI / CD 、微服务、多集群管理、应用管理等功能，帮助企业在云、虚拟化及物理机等异构基础设施上一步升级容器架构，实现应用的敏捷开发与全生命周期管理。

## KubeEdge	
KubeEdge 是一个开源系统，用于将容器化应用程序编排功能扩展到 Edge 的主机。它基于 kubernetes 构建，并为网络应用程序提供基础架构支持。
## KubeDL	
KubeDL 是阿里开源的基于 Kubernetes 的 AI 工作负载管理框架，取自 "Kubernetes-Deep-Learning" 的缩写；旨在使深度学习工作负载能够更轻松、更高效地在 Kubernetes 上运行。
## Kube-OVN	
Kube-OVN 将基于 OVN/OVS 的网络虚拟化方案带入 Kubernetes，提供了针对企业应用场景的高级容器网络编排功能。
## KubeSphere
[KubeSphere](https://kubesphere.com.cn) 是在 Kubernetes 之上构建的开源容器混合云，提供全栈的 IT 自动化运维的能力，简化企业的 DevOps 工作流，帮助企业快速构建一个强大和功能丰富的容器云平台。

![KubeSphere](logo/kubesphere-logo-transparent.png)

## Kratos	
Kratos 是 bilibili 开源的一套 Go 微服务框架，包含大量微服务相关框架及工具。
## JustAuth	
JustAuth 是一个第三方授权登录的工具类库，它可以让我们脱离繁琐的第三方登录 SDK。
## JumpServer	
JumpServer 是全球首款开源的堡垒机，使用 GNU GPL v2.0 开源协议，是符合 4A 规范的运维安全审计系统。
## JuiceFS	
JuiceFS 是基于 Redis 和对象存储（例如 Amazon S3）构建的开源 POSIX 文件系统，针对云原生环境进行了设计和优化。通过使用广泛采用的 Redis 和 S3 作为持久性存储，JuiceFS 可以用作无状态中间件，以使许多应用程序轻松共享数据。
## JingOS	
JingOS 是一款基于 Linux 的 “融合” 开源操作系统，用于平板和移动设备，它可以运行 Linux 和 Android 应用程序，它能让你用手、用笔、用键盘，还有触控板来互动，当你连接键盘和触控板时可以变成桌面模式。

## Jina	
Jina 让你在几分钟内即可构建基于深度学习的搜索即服务。
## JFinal	
JFinal 是基于 Java 语言的极速 WEB + ORM + AOP + Template Engine 框架，其核心设计目标是开发迅速、代码量少、学习简单、功能强大、轻量级、易扩展、Restful。在拥有 Java 语言所有优势的同时再拥有 ruby、python、php 等动态语言的开发效率！为您节约更多时间，去陪恋人、家人和朋友 :)
## Jcseg	
Jcseg 是基于 mmseg 算法的一个轻量级开源中文分词器，同时集成了关键字提取，关键短语提取，关键句子提取和文章自动摘要等功能，并且提供了最新版本的 lucene, solr, elasticsearch 的分词接口。Jcseg 自带了一个 jcseg.properties 文件用于快速配置而得到适合不同场合的分词应用，例如：最大匹配词长、是否开启中文人名识别、是否追加拼音、是否追加同义词等。
## Jboot	
Jboot 是专为大型分布式项目和微服务而生。她是一个基于 jfinal 和 undertow 开发的微服务框架。提供了 AOP、RPC、分布式缓存、限流、降级、熔断、统一配置中心、swagger api 自动生成、Opentracing 数据追踪、metrics 数据监控、分布式 session、代码生成器、shiro 安全控制等功能。

## JPress
JPress 是一个使用 Java 开发的、基于 LGPL 开源的建站 CMS，从2014年开源道现在，已经有 10w+ 网站使用 JPress 进行驱动，其中包括多个政府机构，200+上市公司，中科院、红+字会等。

## J2Paas	
J2PaaS 是吉鼎科技基于 20 年技术沉淀和项目经验而研发的综合性开发平台，覆盖了软件项目需求分析、设计、开发、测试、运行、维护与管理等全过程。
## iView	
iView 是一套基于 Vue.js 的 UI 组件库，主要服务于 PC 界面的中后台产品。
## Hyperf	
Hyperf 是基于 Swoole 4.4+ 实现的高性能、高灵活性的 PHP 协程框架，内置协程服务器及大量常用的组件，性能较传统基于 PHP-FPM 的框架有质的提升，提供超高性能的同时，也保持着极其灵活的可扩展性，标准组件均基于 PSR 标准 实现，基于强大的依赖注入设计，保证了绝大部分组件或类都是 可替换 与 可复用 的。
## HybridOS	
合璧操作系统 (HybridOS) 是一整套专为嵌入式设备打造的快速开发平台
## Hunt Framework	
Hunt framework 是一个由 HuntLabs 推出使用 DLang 语言开发的全栈 web 框架，易用性和完整性都贴近于 Laravel / Django / Spring boot 等主流框架的设计，优势主要体现在部署方面，不需要搭建运行环境就可开启 web 服务。而且 D 语言自身是一个性能极高的编译型语言，我们可以基于 hunt framework 非常简单的开发出高性能的 Web 服务。
## GuiLite	
GuiLite 是 4 千行的图形界面库，可以运行在所有平台（例如：iOS/macOS/WathOS，Android，Linux（ARM/x86-64），Windows（包含 VR），Docker 和 MCU）上；也可以与多种语言（例如： Swift, Java, Javascript, C#, Golang）协同工作。

## GreatSQL	
GreatSQL开源数据库是适用于金融级应用的国内自主MySQL版本，专注于提升MGR可靠性及性能，支持InnoDB并行查询等特性，可以作为MySQL或Percona Server的可选替换，用于线上生产环境，且完全免费并兼容MySQL或Percona Server。

![输入图片说明](logo/GreatSQL-LOGO-横版.png)

## GoPlus	
Go+ 是目前第一个顺应 “三位一体” 发展潮流的编程语言，同时兼顾工程、STEM 教育、数据科学三大领域
## GoFrame	
GoFrame 是一款模块化、高性能、企业级、工程化完备的 Go 基础开发框架。GoFrame 不是一款 WEB/RPC 框架，而是一款通用性的基础开发框架，是 Golang 标准库的一个增强扩展级，包含通用核心的基础开发组件，优点是实战化、模块化、文档全面、模块丰富、易用性高、通用性强、面向团队。
## FydeOS	
FydeOS （原名 Flint OS，于 2018 年 6 月正式更名）基于开源项目「Chromium Project」二次开发，对其底层技术行了修改和优化。以最优化的浏览器平台为基础，加入更多符合中国地区用户习惯和提高用户体验的本地化增强功能，包括对安卓程序的支持、对原生 Linux 应用程序的支持、对常用 Windows 程序的兼容、账号信息和文件云同步以及功能强大的高颗粒度企业集管功能，是一款符合互联网时代需求的云技术操作系统。
## Furion	
Furion 是一个应用程序框架，您可以将它集成到任何 .NET/C# 应用程序中。
## Fregata	
Fregata 是一个基于 Apache Spark 的轻量级、超快速、大规模的机器学习库，并在 Scala 中提供了高级 API。
## FlyFish	
FlyFish（飞鱼）是一个数据可视化编码平台。通过简易的方式快速创建数据模型，通过拖拉拽的形式，快速生成一套数据可视化解决方案。
## FISCO BCOS	
FISCO BCOS 平台是金融区块链合作联盟（深圳）（以下简称：金链盟）开源工作组以金融业务实践为参考样本，在 BCOS 开源平台基础上进行模块升级与功能重塑，深度定制的安全可控、适用于金融行业且完全开源的区块链底层平台。  
## FATE (Federated AI Technology Enabler)	
FATE（Federated AI Technology Enabler）是基于联邦机器学习技术的一个框架，其旨在提供安全的计算框架来支持联邦 AI 生态。FATE 实现了基于同态加密和多方计算（MPC）的安全计算协议，它支持联邦学习架构和各种机器学习算法的安全计算，包括逻辑回归、基于树的算法、深度学习和转移学习。


## fastjson	
fastjson 是一个性能很好的 Java 语言实现的 JSON 解析器和生成器
## Erda	
Erda 是一款基于多云架构的一站式企业数字化平台，为企业提供 DevOps、微服务治理、多云管理以及快数据管理等云厂商无绑定的 IT 服务。
## EMQX	
EMQX 是基于 Erlang/OTP 语言平台开发，支持大规模连接和分布式集群，发布订阅模式的开源 MQTT 消息服务器。
## Element	
Element 是一套为开发者、设计师和产品经理准备的基于 Vue 2.0 的组件库，提供了配套设计资源，帮助你的网站快速成型。
## Egret Engine	
Egret Engine 遵循 HTML5 标准的 2D、3D 引擎，解决了 HTML5 性能问题及碎片化问题，灵活地满足开发者开发 2D 或 3D 游戏的需求，并有着极强的跨平台运行能力。
## Egg.js	
Egg.js 是一个简单的 JS 库。通过观察用户点击情况，你可以使用这个库在网页上添加复活节彩蛋效果。
## EDL	
EDL 是一个弹性深度学习框架，其包含一个 KubernetesController、PaddlePaddle auto-scaler（可以根据集群中的空闲硬件资源改变分布式任务进程的数目）以及一个新的容错计算架构。
## EdgeGallery	
EdgeGallery 是业界首个 5G 边缘计算开源平台。由中国信息通信研究院、中国移动、中国联通、华为、腾讯、紫金山实验室、九州云和安恒信息等八家创始成员发起，其目的是打造一个以 “联接 + 计算” 为特点的 5G MEC 公共平台，实现网络能力（尤其是 5G 网络）开放的标准化和 MEC 应用开发、测试、迁移和运行等生命周期流程的通用化。
## Easegress	
Easegress 是一个云原生流量编排系统

## [Dromara Hutool](https://hutool.cn)    
Hutool 是一个小而全的 Java 工具类库，通过静态方法封装，降低相关 API 的学习成本，提高工作效率，使 Java 拥有函数式语言般的优雅，让 Java 语 言也可以 “甜甜的”。Hutool 对文件、流、加密解密、转码、正则、线程、XML、日期、Http 客户端 等 JDK 方法进行封装，组成各种 Util 工具类。

## [Dromara Raincat](https://gitee.com/dromara/Raincat)        
强一致性分布式事务解决方案，2阶段提交分布式事务中间件    

## [Dromara Hmily](https://gitee.com/dromara/hmily)   
金融级分布式事务解决方案

## [Dromara Myth](https://gitee.com/dromara/myth)     
可靠消息分布式事务解决方案   

## [Dromara LiteFlow](http://liteflow.yomahub.com/)     
轻量，快速，稳定，可编排的组件式规则引擎/流程引擎。 拥有全新设计的DSL规则表达式。 组件复用，同步/异步编排，动态编排，复杂嵌套规则，热部署，平滑刷新规则等等功能，让你加快开发效率！  

## [Dromara TLog](https://tlog.yomahub.com)     
一个轻量级的分布式日志标记追踪神器，10分钟即可接入，自动对日志打标签完成微服务的链路追踪      

## [Dromara Forest](https://forest.dtflyx.com)     
声明式HTTP客户端API框架，让Java发送HTTP/HTTPS请求不再难。它比OkHttp和HttpClient更高层，是封装调用第三方restful api client接口的好帮手，是retrofit和feign之外另一个选择。通过在接口上声明注解的方式配置HTTP请求接口   

## [Dromara MaxKey](https://www.maxkey.top)    
MaxKey单点登录认证系统是业界领先的IAM身份管理和认证产品,支持OAuth2.x、OpenID Connect、SAML2.0、JWT、CAS、SCIM等SSO标准协议     

## [Dromara Cubic](https://cubic.jiagoujishu.com)     
一站式问题定位平台，还在为线上问题而烦恼吗？线程栈监控、线程池监控、动态arthas命令集、依赖分析等等等，助你快速定位问题    

## [Dromara Jpom](https://jpom.io)     
一款简而轻的低侵入式在线构建、自动部署、日常运维、项目监控软件  

## [Dromara Sa-Token](https://sa-token.dev33.cn)       
一个轻量级 Java 权限认证框架，让鉴权变得简单、优雅！—— 登录认证、权限认证、分布式Session会话、微服务网关鉴权、单点登录、OAuth2.0     

## [Dromara Koalas-Rpc](https://gitee.com/dromara/koalas-rpc)     
企业生产级百亿日PV高可用可拓展的RPC框架。理论上并发数量接近服务器带宽，客户端采用thrift协议，服务端支持netty和thrift的TThreadedSelectorServer半同步半异步线程模型，支持动态扩容，服务上下线，权重动态，可用性配置，泛化调用，页面流量统计，泛化调用等，支持trace跟踪等，天然接入cat支持数据大盘展示等，持续为个人以及中小型公司提供可靠的RPC框架技术方案     

## [Dromara HertzBeat](https://hertzbeat.com)    
易用友好的云监控系统。无需Agent，强大自定义监控能力。网站监测，PING连通性，端口可用性，数据库，操作系统，API监控，阈值告警，告警通知(邮件微信钉钉飞书)。   

## [Dromara Image-combiner](https://dromara.gitee.io/image-combiner)    
ImageCombiner是一个专门用于Java服务端图片合成的工具，没有很复杂的功能，简单实用，从实际业务场景出发，提供简单的接口，几行代码即可实现图片拼合（当然用于合成水印也可以），素材上支持图片、文本、矩形三种，支持定位、缩放、旋转、圆角、透明度、颜色、字体、字号、删除线、居中绘制、文本自动换行等特性，足够覆盖图片合成的日常需求。    

## [Dromara Northstar](https://gitee.com/dromara/northstar)     
这是一个最适合部署在服务器7x24运行的量化交易平台； 这是一个可以对交易策略做回测调试的量化交易平台； 这是一个专为程序员、量化交易操盘手打造的量化交易平台； 已对接国内期货CTP交易系统     

## [Dromara Fast-request](https://plugins.sheng90.wang/fast-request)    
IntelliJ plugin Restful Fast Request，IDEA上类似postman的工具    

## [Dromara Easy-es](https://easy-es.cn)        
elasticsearch 国内顶尖elasticsearch搜索引擎框架es ORM框架,索引全自动智能托管,如丝般顺滑,与Mybatis-plus一致的API,屏蔽语言差异,开发者只需要会MySQL语法即可完成对Es的相关操作,零额外学习成本.底层采用RestHighLevelClient,兼具低码,易用,易拓展等特性,支持es独有的高亮,权重,分词,Geo等功能.    

## [Dromara Dynamic-tp](https://dynamictp.cn)       
轻量级动态线程池，内置监控告警功能，基于主流配置中心（已支持Nacos、Apollo、ZK，可通过SPI自定义实现）。    

## [Dromara Mendmix](https://www.jeesuite.com/)        
Mendmix定位是一站式分布式开发架构开源解决方案及云原生架构技术底座。Mendmix提供了数据库、缓存、消息中间件、分布式定时任务、安全框架、网关以及主流产商云服务快速集成能力。基于Mendmix可以不用关注技术细节快速搭建高并发高可用基于微服务的分布式架构。

## [Dromara Gobrs-async](https://async.sizegang.cn)       
一款功能强大、配置灵活、带有全链路异常回调、内存优化、异常状态管理的高性能异步编排框架(多线程管理)。为企业（电商）提供在复杂应用场景下动态任务编排的能力。 针对于复杂场景下，异步线程复杂性、任务依赖性、异常状态难控制性； Gobrs-Async 为此而生。 

## [Dromara x-easypdf](https://xsxgit.gitee.io/x-easypdf)     
一个用搭积木的方式构建pdf的框架（基于pdfbox）

## [Dromara Sureness](https://usthe.com/sureness)       
面向REST API的高性能认证鉴权框架，致力于管理保护API安全  

## DoraemonKit	
DoraemonKit 是滴滴开发的一款功能齐全的客户端（iOS、Android）研发助手
## Dockin	
Dockin 是一个生产级容器平台，提供了一整套私有云容器化的落地方案。涵盖 Kubernetes 集群管理、应用管理、网络、运维工具、开放 API 等组件，用户可以自由搭配使用，定制自己的容器平台。 开源版本从我们生产环境中剥离出来，经过了金融级生产环境的严格验证，是私有化部署的较好方案。


## DG-IoT	
DG-IoT 是国内首款轻量级开源工业物联网平台


## DevStream	
DevStream 是一个开源的 DevOps 工具链管理器，可以通过一个简单的配置文件，将软件研发生命周期中各环节的 DevOps 工具统一管理起来，完成各工具的快速安装部署、工具间整合、最佳实践配置等工作。
## DELTA	
DELTA 主要基于 TensorFlow 构建，能同时支持 NLP (自然语言处理) 和语音任务及数值型特征的训练，整合了包括文本分类、命名实体识别、自然语言推理、问答、序列到序列文本生成、语音识别、说话人验证、语音情感识别等重要算法模型，形成一致的代码组织架构，整体包装统一接口。
## Deepin	
深度操作系统（deepin）包含深度桌面环境（Deepin Desktop Environment）和近 30 款深度原创精品应用，及数款来自开源社区的应用软件，广泛用于支撑用户日常的学习和工作。另外，通过深度商店还能够获得近千款应用软件的支持，满足用户对操作系统的更多应用场景的特定需求。
## datart	
datart 是新一代数据可视化开放平台，支持各类企业数据可视化场景需求，如创建和使用报表、仪表板和大屏，进行可视化数据分析，构建可视化数据应用等。
## Databend	
Databend 是一个具有云原生架构的现代实时数据处理和分析 DBMS，旨在简化数据云。


## CutefishOS	
CutefishOS 是一款基于 Ubuntu 的 Linux 桌面操作系统，其特点是注重简洁、美观和实用性，希望为用户提供舒适的界面设计和更好的用户体验，并满足各种场景的需求。


## Curve	
Curve 是网易开源的高性能、高可用、高可靠分布式存储系统，具有非常良好的扩展性。基于该存储底座可以打造适用于不同应用场景的存储系统，如块存储、对象存储、云原生数据库等。CURVE 的设计开发始终围绕三个理念：一是顺应当前存储硬件设施发展趋势，做到软硬件结合打造顶级的存储产品；二是秉持 “Simple Can be harder than complex”，了解问题本质情况下选择最简单的方案解决问题；三是拥抱开源，在充分调研的前提下使用优秀的开源项目组件，避免造轮子。
![输入图片说明](https://gitee.com/mirrors/curve/raw/master/docs/images/curve-logo1.png)
## Cocos	
Cocos2D-X 是全球知名的开源跨平台游戏引擎，易学易用，目前已经支持 iOS、Android、Windows 桌面、Mac OS X、Linux、BlackBerry、Windows Phone 等平台。Cocos2d-JS 统一了使用 JS 进行开发的开发体验，将 HTML5 引擎与 JSB 的 API 高度统一起来。
## Choerodon	
猪齿鱼 Choerodon 全场景效能平台，提供体系化方法论和协作、测试、DevOps 及容器工具，帮助企业拉通需求、设计、开发、部署、测试和运营流程，一站式提高管理效率和质量。从团队协同到 DevOps 工具链、从平台工具到体系化方法论，猪齿鱼全面满足协同管理与工程效率需求，贯穿端到端全流程，助力团队效能更快更强更稳定。
## Chameleon	
Chameleon 是一个跨端开发框架。虽然不同各端环境千变万化，但万变不离其宗的是 MVVM 架构思想，Chameleon 目标是让 MVVM 跨端环境大统一。

## [BlueKing](/蓝鲸智云配置平台社区.md)	
蓝鲸配置平台（bk-cmdb）是一款面向资产与应用的 CMDB，致力于为企业 IT 自动化运维和 DevOps 的落地提供最基础的元数据；结合自动化的数据采集、管理、消费能力，将企业应用的配置数据和资产数据进行深度融合，形成一套可自生长的、面向应用的 CMDB。
## Beetl	
Beetl 是新一代 Java 模板引擎典范。相对于其他 java 模板引擎，具有功能齐全，语法直观，性能超高，开发和维护模板有很好的体验。是新一代的模板引擎。
## Beego	
beego 是一个快速开发 Go 应用的 HTTP 框架，他可以用来快速开发 API、Web 及后端服务等各种应用，是一个 RESTful 的框架，主要设计灵感来源于 tornado、sinatra 和 flask 这三个框架，但是结合了 Go 本身的一些特性（interface、struct 嵌入等）而设计的一个框架。
## BAETYL	
Baetyl 是 Linux Foundation Edge 旗下项目，旨在将云计算能力拓展至用户现场，提供临时离线、低延时的计算服务，包括设备接入、消息路由、消息远程同步、函数计算、设备信息上报、配置下发等功能。Baetyl 和 智能边缘 BIE（Baidu-IntelliEdge）云端管理套件配合使用，通过在云端进行智能边缘核心设备的建立、存储卷创建、服务创建、函数编写，然后生成配置文件下发至 Baetyl 本地运行包，整体可达到 边缘计算、云端管理、边云协同 的效果，满足各种边缘计算场景。
## Avue	
Avue.js 是基于现有的 element-ui 库进行的二次封装，从而简化一些繁琐的操作，核心理念为数据驱动视图，主要的组件库针对 table 表格和 form 表单场景，同时衍生出更多企业常用的组件，达到高复用，容易维护和扩展的框架，同时内置了丰富了数据展示组件，让开发变得更加容易。
## Arthas	
Arthas（阿尔萨斯）是阿里巴巴开源的 Java 诊断工具，深受开发者喜爱。


## Apache SkyWalking	
SkyWalking 是一款开源的应用性能监控系统，能力包括指标监控、分布式追踪、分布式系统性能诊断。
## Apache ShardingSphere	
Apache ShardingSphere 是一套开源的分布式数据库中间件解决方案组成的生态圈，它由 JDBC、Proxy 和 Sidecar（规划中）这 3 款相互独立，却又能够混合部署配合使用的产品组成。 它们均提供标准化的数据分片、分布式事务和数据库治理功能，可适用于如 Java 同构、异构语言、云原生等各种多样化的应用场景。

## Apache ShenYu	
Apache ShenYu 是一款用于微服务代理，协议转换，API治理的API网关。

## Apache SeaTunnel

<img src="logo/SeaTunnel.png" width="500">
	
SeaTunnel 是一个非常易用的支持海量数据实时同步的超高性能分布式数据集成平台，每天可以稳定高效同步数百亿数据，已在近百家公司生产上使用。

## [Apache RocketMQ](/Apache%20RocketMQ%20社区.md)	

Apache RocketMQ 是一款开源的分布式消息系统，基于高可用分布式集群技术，提供低延时的、高可靠的消息发布与订阅服务。

## Apache Pegasus	
Pegasus 是小米云存储团队开发的一个分布式 Key-Value 存储系统，最初的动机是弥补 HBase 在可用性和性能上的不足。Pegasus 系统的 Server 端完全采用 C++ 语言开发，使用 PacificA 协议支持强一致性，使用 RocksDB 作为单机存储引擎。

## [Apache Linkis](/Apache%20Linkis%20社区.md)
	 
Apache Linkis 是微众银行开源的大数据计算中间件，上层应用只需对接 Linkis 提供的标准接口，就能连接到 MySQL/Spark/Hive/Presto/Flink 等各种底层计算存储引擎，并实现上层应用间的互通。

## Apache Kyuubi	
Kyuubi 是一个高性能的通用 JDBC 和 SQL 执行引擎，建立在 Apache Spark 之上。通过 Kyuubi，用户能够像处理普通数据一样处理大数据，目标是成为数据仓库和数据湖的 “开箱即用” 工具。
![输入图片说明](https://svn.apache.org/repos/asf/comdev/project-logos/originals/kyuubi-1.svg)
## Apache Kylin	
Apache Kylin 是一个开源的分布式的 OLAP 分析引擎，来自 eBay 公司开发，基于 Hadoop 提供 SQL 接口和 OLAP 接口，支持 TB 到 PB 级别的数据量。
## Apache Kvrocks	
Kvrocks 是基于 RocksDB 之上兼容 Redis 协议的 NoSQL 存储服务，设计目标是提供一个低成本以及大容量的 Redis 服务，作为 Redis 在大数据量场景的互补服务，选择兼容 Redis 协议是因为简单易用且业务迁移成本低。复制和存储的设计受到 rocksplicator 和 blackwidow 的启发。
## Apache IoTDB	
IoTDB 是针对时间序列数据收集、存储与分析一体化的数据管理引擎。它具有体量轻、性能高、易使用的特点，完美对接 Hadoop 与 Spark 生态，适用于工业物联网应用中海量时间序列数据高速写入和复杂分析查询的需求。

## Apache InLong	
Apache InLong（原 Apache TubeMQ 项目更名）是腾讯在 2013 年自研的分布式消息中间件系统，专注服务大数据场景下海量数据的高性能存储和传输，较之于众多明星的开源 MQ 组件，TubeMQ 在海量实践（稳定性 + 性能）和低成本方面有着比较好的核心优势。

## Apache HAWQ	
HAWQ 是一个 Hadoop 原生大规模并行 SQL 分析引擎，针对的是分析性应用。和其他关系型数据库类似，接受 SQL，返回结果集。
Apache HAWQ 具有大规模并行处理很多传统数据库以及其他数据库没有的特性及功能。

## Apache Flink	
Apache Flink 是高效和分布式的通用数据处理平台，是一个流批一体分析引擎。
Apache Flink 声明式的数据分析开源系统，结合了分布式 MapReduce 类平台的高效，灵活的编程和扩展性。同时在并行数据库发现查询优化方案。

## Apache EventMesh	
EventMesh 是一个动态的云原生事件驱动架构基础设施，用于分离应用程序和后端中间件层，它支持广泛的用例，包括复杂的混合云、使用了不同技术栈的分布式架构。

## [Apache ECharts](/Apache%20ECharts%20社区.md)
	
Apache ECharts 是一款基于 JavaScript 的数据可视化图表库，提供直观、生动、可交互、可个性化定制的数据可视化图表。

## Apache Dubbo-go	

7 年来，社区同学⼀起实现了 dubbogo 与 dubbo 所有版本的对⻬，实 现了与 dubbo、Spring Cloud、gRPC ⽣态的互联互通，dubbogo 3.0 在 K8s、柔性服务等云原⽣⽅向持续探索反哺 dubbo，构建 proxyless Dubbo Mesh，达成了 "bridging the gap between Java and Go" 初始⽬标。目前正推进 dubbo-go/dubbo-go-pixiu 等社区项目，打造下一代 Dubbo Mesh 生态；还在 与 RocketMQ 合作，在 RocketMQ 之上构建⾦融级的服务通信系统，达成服务通信、通信路径日志回溯 和 构建跨网络安全通信的统一；与腾讯 Polaris 以及阿里 OpenSergo 等项目合作，建立新一代微服务标准体系。通过不断拓展 Dubbo ⽣态的边界，达成 "bridging the gap between Dubbo and X" 的新使命。

![输入图片说明](logo/dubbogo.png)


## Apache Dubbo	
Dubbo 是阿里巴巴公司开源的一个高性能优秀的服务框架，使得应用可通过高性能的 RPC 实现服务的输出和输入功能，可以和 Spring 框架无缝集成。
## Apache Doris	
Apache Doris 是一个基于 MPP 架构的高性能、实时的分析型数据库，以极速易用的特点被人们所熟知，仅需亚秒级响应时间即可返回海量数据下的查询结果，不仅可以支持高并发的点查询场景，也能支持高吞吐的复杂分析场景。基于此，Apache Doris 能够较好的满足报表分析、即席查询、统一数仓构建、数据湖联邦查询加速等使用场景，用户可以在此之上构建用户行为分析、AB 实验平台、日志检索分析、用户画像分析、订单分析等应用。
![输入图片说明](logo/logo.jpeg)

## [Apache DolphinScheduler](/Apache%20DolphinScheduler%20社区.md)
![DolphinScheduler](logo/DolphinScheduler.png)	

Apache DolphinScheduler 是一个新一代分布式大数据工作流任务调度系统，致力于“解决大数据任务之间错综复杂的依赖关系，整个数据处理开箱即用”。

## Apache DevLake	
Dev Lake 可将所有的 DevOps 数据以实用、个性化、可扩展的视图呈现。
## Apache brpc	
Apache brpc是一个用于构建可靠和高性能服务的工业级RPC框架。"brpc"的含义是"better RPC"。

<img src="logo/brpc-logo.png" width="400">


## Apache APISIX	
Apache APISIX 是一个动态、实时、高性能的开源 API 网关，提供负载均衡、动态上游、灰度发布、服务熔断、身份认证、可观测性等丰富的流量管理功能。
作为 API 网关，Apache APISIX 可以帮助企业快速、安全地处理 API 和微服务流量，可应用于网关、Kubernetes Ingress 和服务网格等场景。目前已被普华永道数据安全团队、腾讯蓝军、平安银河实验室、爱奇艺 SRC 和源堡科技安全团队等专业网络安全机构测试，并得到了高度认可。

<img src="logo/apache-apisix.png" width="400">

## Ant Design	
Ant Design，一套企业级 UI 设计语言和 React 组件库。

## OpenAnolis 龙蜥操作系统开源社区

龙蜥社区（OpenAnolis）成立于 2020 年 9 月，是面向国际的自主开源原生社区，也是一个操作系统开源创新平台，由阿里云、统信、intel、Arm、龙芯和三大运营商等多家操作系统厂商、芯片公司、云计算公司共同发起，已有17家理事单位、近200家合作伙伴参与社区共建。龙蜥社区旨在通过软硬协同，实现用一个操作系统平台解决不通过场景、不同芯片平台的系统碎片化问题，在“云边端”无缝切换，并提供10年技术支持。目前，龙蜥操作系统已在阿里云全面上线，总装机量达百万量级，阿里云、统信、移动、中科方德5家企业也已基于龙蜥操作系统发布商业版本。

<img src="logo/openanolis.png" width="500">


## Angel ML	
Angel 是一个基于参数服务器（Parameter Server）理念开发的高性能分布式机器学习平台，它基于腾讯内部的海量数据进行了反复的调优，并具有广泛的适用性和稳定性，模型维度越高，优势越明显。 Angel 由腾讯和北京大学联合开发，兼顾了工业界的高可用性和学术界的创新性。
## AliOS Things	
AliOS Things 是 AliOS 家族旗下的、面向 IoT 领域的、高可伸缩的物联网操作系统。
AliOS Things 致力于搭建云端一体化 IoT 基础设施，具备极致性能、极简开发、云端一体、丰富组件、安全防护等关键能力，并支持终端设备连接到阿里云 Link，可广泛应用在智能家居、智慧城市、新出行等领域。
## Airtest	
Airtest 是一个跨平台的、基于图像识别的 UI 自动化测试框架，适用于游戏和 App，支持平台有 Windows、Android 和 iOS。Airtest 提供了跨平台的 API，包括安装应用、模拟输入、断言等。 
基于图像识别技术定位 UI 元素，你无需嵌入任何代码即可进行自动化测试。 测试脚本运行后可以自动生成详细的 HTML 测试报告，让你迅速定位失败的测试点。
## Adlik	
Adlik 是深度学习模型的端到端优化框架。Adlik 的目标是在云和嵌入式环境中加速深度学习推理过程。
## ActFramework	
ActFramework 是一种简洁易用，具有强大表达力的 Java MVC 全栈框架
## OneOS
OneOS 是中国移动针对物联网领域推出的轻量级操作系统，具有可裁剪、跨平台、低功耗、高安全等特点，支持 ARM Cortex-M/R/A、MIPS、RISC-V 等主流 CPU 架构，兼容 POSIX、CMSIS 等标准接口，支持 Micropython 语言开发，提供图形化开发工具，能够有效提高开发效率并降低开发成本，帮助客户开发稳定可靠、安全易用的物联网应用。

## CloudWeGo

CloudWeGo 是一套开源微服务中间件集合，具备高性能、强扩展性和高可靠的特点，专注于解决微服务通信与治理的难题，满足不同业务在不同场景的诉求。

## Hango

Hango 是网易数帆开源的一个基于 Envoy 构建的高性能、可扩展、功能丰富的云原生API网关。Hango 提供请求代理、动态路由、负载均衡、限流、熔断、健康检查、安全防护等功能，可用于微服务网关、七层负载均衡、Kubernetes Ingress、Serverless网关等应用场景。

![输入图片说明](logo/hango.png)

## KubeCube

KubeCube是一个开源的企业级容器平台，为企业提供kubernetes资源可视化管理以及统一的多集群多租户管理功能。KubeCube可以简化应用部署、管理应用的生命周期和提供丰富的监控和日志审计功能，帮助企业快速构建一个强大和功能丰富的容器云平台，并增强 DevOps 团队的能力。

![输入图片说明](logo/kubecube.png)

## KubeDiag

KubeDiag 是一个云原生诊断运维编排框架，支持通过 Kubernetes 提供的标准化能力，将问题诊断和故障运维工作中的专业经验进行沉淀，助力用户实现 DevOps 流程中监控、运维、诊断的全自动化管理以及场景化的交互式根因定位。

![输入图片说明](logo/kubediag.png)

## Loggie

Loggie 是一个基于 Golang 的轻量级、高性能、云原生日志采集 Agent 和中转处理 Aggregator，支持多 Pipeline 和组件热插拔。

![输入图片说明](logo/loggie.png)

## DevUI

DevUI 是一个面向企业中后台产品的开源前端解决方案，有一套完善的 DevUI Design 设计体系，并实现了 Ng DevUI、Vue DevUI、React DevUI 三大框架的组件库，以及与之配套的 Admin 系统、字体图标库、VSCode 插件等组件库生态产品。
Ng DevUI 目前已支持 66 个功能丰富的组件，包含甘特图、象限图、仪表盘、拖拽、分割器等特色组件，表格、树、下拉选择框、日期选择器、穿梭框等组件支持高性能的虚拟滚动，支持组件按需引入、国际化、主题定制、全局配置、TypeScript等。

<img src="logo/devui-logo.svg" width="500">

## Tapdata
Tapdata 是 Tapdata Inc.的开源项目，是新一代的实时数据平台，通过把核心数据实时集中到中央化数据平台的方式并通过 API 或者反向同步方式，为下游的交互式应用，微服务或交互式分析提供新鲜实时的数据。
